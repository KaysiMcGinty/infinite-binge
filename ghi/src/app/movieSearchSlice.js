import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  value: "",
};

export const movieSearchSlice = createSlice({
  name: "movieSearch",
  initialState,
  reducers: {
    reset: (state) => {
      state.value = initialState.value;
    },
    search: (state, action) => {
      state.value = action.payload;
    },
  },
});

export const { reset, search } = movieSearchSlice.actions;

export const selectSearchKeyword = (state) => state.movieSearch.value;

export default movieSearchSlice.reducer;
