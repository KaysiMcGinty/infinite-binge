import React, { useState } from 'react';
import { useNavigate, Link } from 'react-router-dom';
import { useSignupMutation } from './app/apiSlice';
import logoIcon from './logoIcon.png';

function SignUp() {
  const navigate = useNavigate();
  const [signup, { isError, error }] = useSignupMutation();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [username, setUsername] = useState('');
  const [isChecked, setIsChecked] = useState(false);
  const [validationError, setValidationError] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (password !== confirmPassword) {
      setValidationError('Passwords do not match');
      return;
    }
    if (!username || !email || !password || !confirmPassword || !isChecked) {
      setValidationError('Please fill out all the fields');
      return;
    }

    const result = await signup({ username, email, password });
    if (result.error) {
      return;
    }
    if (result) {
      return navigate('/home');
    }
  };

  return (
    <div className="vh-100">
      <div className="logo" style={{ marginBottom: "-50px" }}>
        <img
          className="d-block mx-auto logo scale-animation"
          src={logoIcon}
          alt=""
          width="200"
        />
      </div>
      <h1 className="section-title text-center">Sign Up</h1>
      <div className="row">
        <div className="col-md-6 offset-md-3 d-flex flex-column align-items-center">
          <form onSubmit={handleSubmit} className="w-100">
            <div className="mb-3">
              <label htmlFor="Signup_username" className="form-label">
                Username
              </label>
              <input
                type="text"
                className="form-control"
                id="Signup_username"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                required
              />
            </div>
            <div className="mb-3">
              <label htmlFor="Signup_email" className="form-label">
                Email
              </label>
              <input
                type="email"
                className="form-control"
                id="Signup_email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
            </div>
            <div className="mb-3">
              <label htmlFor="Signup_password" className="form-label">
                Password
              </label>
              <input
                type="password"
                className="form-control"
                id="Signup_password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </div>
            <div className="mb-3">
              <label htmlFor="Signup_confirmPassword" className="form-label">
                Confirm Password
              </label>
              <input
                type="password"
                className="form-control"
                id="Signup_confirmPassword"
                value={confirmPassword}
                onChange={(e) => setConfirmPassword(e.target.value)}
                required
              />
            </div>
            <div className="mb-3">
              <label>
                <input
                  type="checkbox"
                  checked={isChecked}
                  onChange={(e) => setIsChecked(e.target.checked)}
                  required
                />
                <span style={{ marginLeft: '0.5rem' }}>
                  I agree to Binge
                </span>
              </label>
            </div>
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
            {validationError && <div className="text-danger">{validationError}</div>}
            {isError && <p className="text-danger">Error: {error.data.detail}</p>}
          </form>
          <p>
            Already have an account? <Link to="/login">Login in here</Link>
          </p>
        </div>
      </div>
    </div>
  );
}

export default SignUp;
