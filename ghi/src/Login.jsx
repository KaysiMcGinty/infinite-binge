import { useLoginMutation } from "./app/apiSlice";
import { useState } from "react";
import { useNavigate, Link } from "react-router-dom";
import LogoIcon from './logoIcon.png'

function Login() {
  const navigate = useNavigate();
  const [login] = useLoginMutation();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(null);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const form = await login({ username, password });
    if (form.error)
      setError(form.error);
    else return navigate("/home")
  };

  return (
    <div className="vh-100">
      <div className="logo" style={{ marginBottom: "-50px" }}>
        <img
          className="d-block mx-auto logo scale-animation"
          src={LogoIcon}
          alt=""
          width="200"
        />
      </div>
      <h1 className="section-title text-center">Login</h1>
      <div className="row">
        <div className="col-md-6 offset-md-3 d-flex flex-column align-items-center">
          <div>
            {error ? <p className="alert alert-danger" role="alert">Invalid login credentials. Please try again or Sign Up to create a new account</p> : null}
          </div>
          <form onSubmit={handleSubmit} className="w-100">
            <div className="mb-3">
              <label htmlFor="Login_username" className="form-label">
                Email
              </label>
              <input
                type="text"
                className="form-control"
                id="Login_username"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                required
              />
            </div>
            <div className="mb-3">
              <label htmlFor="Login_password" className="form-label">
                Password
              </label>
              <input
                type="password"
                className="form-control"
                id="Login_password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </div>
            <div className="mb-3">
              <label>
                <input type="checkbox" />
                <span style={{ marginLeft: '0.5rem' }}>Remember Me</span>
              </label>
            </div>
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </form>
          <p>
            Don't have an account? <Link to="/signup">Sign up here!</Link>
          </p>
        </div>
      </div>
    </div>
  );
}

export default Login;
