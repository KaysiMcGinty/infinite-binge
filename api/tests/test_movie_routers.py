from fastapi.testclient import TestClient
from main import app
from queries.movie_queries import MovieQueries


client = TestClient(app)


class FakeMovieQueries:
    def get_all(self):
        return []

    def get_by_search(self, keyword: str):
        return [
            {
                "id": 37135,
                "original_title": "Tarzan",
                "overview": "Tarzan was a small orphan who [..]",
                "poster_path": "/ceGk1VcuI8pSXQhSlnqsqTFCPLD.jpg",
                "vote_average": 7,
            },
        ]

    def get_by_id(self, movie_id: str):
        return {
            "adult": False,
            "backdrop_path": "/hZkgoQYus5vegHoetLkCJzb17zJ.jpg",
            "belongs_to_collection": None,
            "budget": 63000000,
            "genres": [
                {"id": 18, "name": "Drama"},
                {"id": 53, "name": "Thriller"},
                {"id": 35, "name": "Comedy"},
            ],
            "homepage": "http://www.foxmovies.com/movies/fight-club",
            "id": 550,
            "imdb_id": "tt0137523",
            "original_language": "en",
            "original_title": "Fight Club",
            "overview": "A ticking-time-bomb insomniac and a slippery [...]",
            "popularity": 69.272,
            "poster_path": "/pB8BM7pdSp6B6Ih7QZ4DrQ3PmJK.jpg",
            "production_companies": [
                {
                    "id": 508,
                    "logo_path": "/7cxRWzi4LsVm4Utfpr1hfARNurT.png",
                    "name": "Regency Enterprises",
                    "origin_country": "US",
                },
                {
                    "id": 711,
                    "logo_path": "/tEiIH5QesdheJmDAqQwvtN60727.png",
                    "name": "Fox 2000 Pictures",
                    "origin_country": "US",
                },
                {
                    "id": 20555,
                    "logo_path": "/hD8yEGUBlHOcfHYbujp71vD8gZp.png",
                    "name": "Taurus Film",
                    "origin_country": "DE",
                },
                {
                    "id": 54051,
                    "logo_path": None,
                    "name": "Atman Entertainment",
                    "origin_country": "",
                },
                {
                    "id": 54052,
                    "logo_path": None,
                    "name": "Knickerbocker Films",
                    "origin_country": "US",
                },
                {
                    "id": 4700,
                    "logo_path": "/A32wmjrs9Psf4zw0uaixF0GXfxq.png",
                    "name": "The Linson Company",
                    "origin_country": "US",
                },
                {
                    "id": 25,
                    "logo_path": "/qZCc1lty5FzX30aOCVRBLzaVmcp.png",
                    "name": "20th Century Fox",
                    "origin_country": "US",
                },
            ],
            "production_countries": [
                {"iso_3166_1": "US", "name": "United States of America"}
            ],
            "release_date": "1999-10-15",
            "revenue": 100853753,
            "runtime": 139,
            "spoken_languages": [
                {
                    "english_name": "English",
                    "iso_639_1": "en",
                    "name": "English",
                }
            ],
            "status": "Released",
            "tagline": "Mischief. Mayhem. Soap.",
            "title": "Fight Club",
            "video": False,
            "vote_average": 8.434,
            "vote_count": 26481,
        }


def test_get_popular_movies():
    # Arrange
    app.dependency_overrides[MovieQueries] = FakeMovieQueries

    # Act
    res = client.get("/api/movies/popular/")
    data = res.json()

    # Assert
    assert res.status_code == 200
    assert data == {"movies": []}

    # Cleanup
    app.dependency_overrides = {}


def test_get_searched_movies():
    # ARRANGE
    app.dependency_overrides[MovieQueries] = FakeMovieQueries

    # ACT
    res = client.get("api/movies/search/Tarzan")
    data = res.json()

    # ASSERT
    assert res.status_code == 200
    assert data["movies"][0]["original_title"] == "Tarzan"

    # CLEAN UP
    app.dependency_overrides = {}


def test_get_movie_details():
    # Arrange
    app.dependency_overrides[MovieQueries] = FakeMovieQueries

    # Act
    res = client.get("/api/movies/550")
    data = res.json()

    # Assert
    assert res.status_code == 200
    assert data["movie"]["id"] == 550

    # Cleanup
    app.dependency_overrides = {}
