from pydantic import BaseModel
from typing import List


class FavoriteIn(BaseModel):
    movie_id: str


class FavoriteOut(BaseModel):
    movie_id: str
    user_id: str


class FavoritesList(BaseModel):
    favorites: List[FavoriteOut]
